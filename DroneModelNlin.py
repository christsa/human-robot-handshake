import numpy as np
from gym import spaces


class DroneModelNlin(object):
    def __init__(self, dt, params):

        self.reset_state = np.array([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0])
        self.state = self.reset_state
        self.old_state = self.state

        self.down_state_limit = np.array([-1.0, -5.0, 0.0, -10.0, -10.0, -np.pi / 2, -np.pi / 2])
        self.up_state_limit = np.array([10.0, 5.0, 5.0, 10.0, 10.0, np.pi / 2, np.pi / 2])

        self.observation_space = spaces.Box(self.down_state_limit[2:5], self.up_state_limit[2:5])
        self.state_size = 3

        self.down_actions_limit = np.array([-np.pi / 6, -np.pi / 6, -2.0])
        self.up_action_limit = np.array([np.pi / 6, np.pi / 6, 2.0])
        self.action_size = 3

        self.action_space = spaces.Box(self.down_actions_limit, self.up_action_limit)

        self.alpha = params['alpha']
        self.C = params['C']
        self.dt = dt

    def robot_model(self, control_input):

        pos = self.state[0:3]
        vel = self.state[3:5]
        angles = self.state[5:7]

        angle_k = control_input[0:2] + self.alpha*(angles - control_input[0:2])
        acc = np.array([-np.tan(angles[1]), np.tan(angles[0])])*9.81 - self.C*vel
        pos_k = pos[0:2] + vel*self.dt + 0.5*self.dt**2*acc
        pos_k_z = np.array([pos[2] + control_input[2]*self.dt])
        vel_k = vel + self.dt*acc

        self.old_state = self.state
        self.state = np.concatenate((pos_k, pos_k_z, vel_k, angle_k))

        return self.state[2:5]  # todo: maybe old state

    def step(self, control_input):

        control_input = control_input/10.0

        for it_controls in range(self.action_size):
            if control_input[it_controls] > self.up_action_limit[it_controls]:
                control_input[it_controls] = self.up_action_limit[it_controls]
            elif control_input[it_controls] < self.down_actions_limit[it_controls]:
                control_input[it_controls] = self.down_actions_limit[it_controls]

        observation = self.robot_model(control_input)

        # here goes the cost calcuation based on spline

        ec_coefficient = 0.1*15.0
        vel_coefficient = 0.1*2.0
        # vel_coefficient = 2.0

        # survive_cost = 1.0
        # ec_reward = - ec_coefficient*np.sum((np.array([.0, 1.0]) - self.state[1:3])**2) # TODO: this is for a straight spline, we need to find optimisation to find reward for arbitrary spline
        # vel_reward = - vel_coefficient*(2.0 - self.state[3])**2
        # ctrl_cost = - 0.001*2.0 * np.square(control_input).sum()
        #
        # reward = vel_reward + survive_cost + ctrl_cost# + vel_reward + ctrl_cost # + survive_cost

        survive_cost = 1.0
        vel_reward = (self.state[0] - self.old_state[0])/self.dt
        ec_reward = - 10*0.5*np.sum((np.array([.0, 1.0]) - self.state[1:3])**2)
        ctrl_cost = - 0.001*2.0 * np.square(control_input).sum()

        reward = vel_reward + survive_cost + ctrl_cost + ec_reward

        # TODO: cost does not penalize if drone goes out of range because its very far from line, range also could be smaller
        # TODO: we could penalize if control signal out of bounds, but for now only ctrl_cost as in mujoco envs

        # check if outside of the box so we can interrupt training

        done = False
        for it_states in range(self.state_size):
            if self.state[it_states] > self.up_state_limit[it_states]:
                done_it = True
            elif self.state[it_states] < self.down_state_limit[it_states]:
                done_it = True
            else:
                done_it = False

            if done_it:
                done = True
                break

        # if done:
        #     reward = -1000.0

        info = {'state': self.state}

        return observation, reward, done, info

    def reset(self):
        self.state = self.reset_state
        return self.state[2:5]
