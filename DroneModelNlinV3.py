import numpy as np
from gym import spaces
import scipy.interpolate as si


class DroneModelNlinV3(object):
    def __init__(self, dt, params):

        self.reset_state = np.array([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.state = self.reset_state
        self.old_state = self.state

        self.spline_degree = 3
        self.prediction_len = params['prediction_len']
        self.action_sequence = np.zeros(self.prediction_len + self.prediction_len + self.prediction_len, dtype=np.int)
        self.spline_func, self.quad_path, self.spl_projections, self.max_t = \
            DroneModelNlinV3.get_b_spline(self.action_sequence, self.spline_degree)
        self.spline_der = self.spline_func.derivative()
        self.t_param = 0.0
        self.t_old = 0.0
        self.ref_ind_cur = 0

        self.ref_pose = np.array([0.0, 0.0, 1.0])

        self.down_state_limit = np.array([-5.0, -5.0, -5.0, -10.0, -10.0, -10.0, -np.pi / 2, -np.pi / 2, -100.0])
        self.up_state_limit = np.array([10.0, 5.0, 6.0, 10.0, 10.0, 10.0, np.pi / 2, np.pi / 2, 100.0])

        # self.down_ob_limit = np.concatenate((np.zeros(self.prediction_len), np.array([0.0, -10.0, -10.0])))
        # self.up_ob_limit = np.concatenate((8.0*np.ones(self.prediction_len), np.array([5.0, 10.0, 10.0])))
        # self.down_ob_limit = np.concatenate((np.zeros(self.prediction_len), np.array([-10.0, -10.0, -30.0])))
        # self.up_ob_limit = np.concatenate((8.0*np.ones(self.prediction_len), np.array([10.0, 10.0, 30.0])))
        self.down_ob_limit = np.concatenate((np.zeros(self.prediction_len), np.array([-5.0, -10.0, -10.0, -10.0])))
        self.up_ob_limit = np.concatenate((8.0*np.ones(self.prediction_len), np.array([6.0, 10.0, 10.0, 10.0])))
        self.observation_space = spaces.Box(self.down_ob_limit, self.up_ob_limit)
        self.state_size = 4 + self.prediction_len

        # self.down_ob_limit = np.concatenate((np.zeros(self.prediction_len), np.array([0.0, -10.0, -10.0]), np.array([-5.0, -5.0])))
        # self.up_ob_limit = np.concatenate((8.0 * np.ones(self.prediction_len), np.array([5.0, 10.0, 10.0]), np.array([10.0, 5.0])))
        # self.observation_space = spaces.Box(self.down_ob_limit, self.up_ob_limit)
        # self.state_size = 3 + self.prediction_len + 2

        # self.down_ob_limit = np.concatenate((np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0]), np.array([0.0, -10.0, -10.0])))
        # self.up_ob_limit = np.concatenate((np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0]), np.array([5.0, 10.0, 10.0])))
        # self.observation_space = spaces.Box(self.down_ob_limit, self.up_ob_limit)
        # self.state_size = 6 + 3

        self.down_actions_limit = np.array([-np.pi / 6, -np.pi / 6, -100.0])
        self.up_action_limit = np.array([np.pi / 6, np.pi / 6, 100.0])
        self.action_size = 3

        self.action_space = spaces.Box(self.down_actions_limit, self.up_action_limit)

        self.mass = params['mass']
        self.alpha = params['alpha']
        self.C = params['C']
        self.dt = dt

    def robot_model(self, control_input):

        pos = self.state[0:3]
        vel = self.state[3:6]
        angles = self.state[6:8]
        acc_z = self.state[8]

        angle_k = control_input[0:2] + self.alpha*(angles - control_input[0:2])
        # acc_z = (control_input[2] - self.mass*9.8)/self.mass
        acc_z = control_input[2] / self.mass
        acc = np.array([-np.tan(angles[1]), np.tan(angles[0])])*(control_input[2] + 9.8*self.mass)/self.mass - self.C*vel[0:2]
        pos_k = pos[0:2] + vel[0:2]*self.dt + 0.5*self.dt**2*acc
        pos_k_z = np.array([pos[2] + vel[2]*self.dt + 0.5*self.dt**2*acc_z])
        vel_k = vel[0:2] + self.dt*acc
        vel_k_z = np.array([vel[2] + self.dt*acc_z])

        self.old_state = self.state
        self.state = np.concatenate((pos_k, pos_k_z, vel_k, vel_k_z, angle_k, np.array([acc_z])))

        ## add noise to state

        # pos_dev = 0.01
        # vel_dev = 0.01
        # angle_dev = 0.00
        #
        # self.state[0:3] += pos_dev * np.random.normal(size=3)
        # self.state[3:5] += vel_dev * np.random.normal(size=2)
        # self.state[5:7] += angle_dev * np.random.normal(size=2)

        return self.state[2:6]

    def step(self, control_input):

        # print(control_input)
        # print(control_input.__class__)
        control_input_2 = control_input[0:2] / 10.0
        control_input = np.concatenate((control_input_2, np.array([control_input[2]/5.0])))
        # print(control_input)
        # print(control_input.__class__)
        # control_input[0:2] /= 10.0 # control_input[0:2]/10.0
        # control_input[2] /= 10.0 # control_input[2]/10.0
        # print(control_input)
        # print(control_input.__class__)

        # control_input[0] /= 10.0  # control_input[0:2]/10.0
        # control_input[1] /= 10.0
        # control_input[2] /= 10.0  # control_input[2]/10.0

        for it_controls in range(self.action_size):
            if control_input[it_controls] > self.up_action_limit[it_controls]:
                control_input[it_controls] = self.up_action_limit[it_controls]
            elif control_input[it_controls] < self.down_actions_limit[it_controls]:
                control_input[it_controls] = self.down_actions_limit[it_controls]

        observation = self.robot_model(control_input)
        t_closest, distance = self.get_closest_t()

        vel_t = (t_closest - self.t_old)/self.dt
        self.t_old = t_closest

        der_at_t = self.spline_der(t_closest)
        der_at_t = der_at_t/np.linalg.norm(der_at_t)

        ref_actions = self.get_ref_actions()
        observation = np.concatenate((ref_actions.astype(dtype=np.float32), observation))
        # d_pos = self.state[0:3] - self.spline_func(t_closest)
        # spline_measurement = np.concatenate((d_pos, der_at_t))
        # observation = np.concatenate((spline_measurement, observation))

        # d_pos = self.state[0:3] - self.spl_projections[self.ref_ind_cur]
        # d_pos = self.state[0:3] - self.ref_pose
        # observation = np.concatenate((observation, d_pos[0:2]))

        # here goes the cost calcuation based on spline

        survive_cost = 0.0
        vel_reward = - 0.1 * (1.5 - np.dot(der_at_t[0:2], self.state[3:5])) ** 2
        # vel_reward = 0.1 * 1 * vel_t
        # vel_reward = 0.0
        ec_reward = - 5 * distance ** 2
        # print(ec_reward)
        ctrl_cost = -0.01 * 2.0 * np.square(control_input).sum()
        # ctrl_cost = 0.0

        reward = vel_reward + survive_cost + ctrl_cost + ec_reward

        # TODO: cost does not penalize if drone goes out of range because its very far from line, range also could be smaller
        # TODO: we could penalize if control signal out of bounds, but for now only ctrl_cost as in mujoco envs

        # check if outside of the box so we can interrupt training

        done = False
        if distance > 3.5:
            done = True
        elif self.ref_ind_cur >= (np.shape(self.action_sequence)[0] - 1):
            done = True
        else:
            for it_states in range(np.shape(self.up_state_limit)[0]):
                if self.state[it_states] > self.up_state_limit[it_states]:
                    done_it = True
                elif self.state[it_states] < self.down_state_limit[it_states]:
                    done_it = True
                else:
                    done_it = False

                if done_it:
                    done = True
                    break

        info = {'state': self.state}
        info['t_min'] = t_closest

        return observation, reward, done, info

    def reset(self):
        self.state = self.reset_state
        ref_actions = self.get_ref_actions()
        observation = np.concatenate((ref_actions.astype(dtype=np.float32), self.state[2:6]))
        self.ref_ind_cur = 0
        self.ref_pose = np.array([0.0, 0.0, 1.0])
        # distance = 0.0
        # d_pos = self.state[0:3] - self.spline_func(0.0)
        # der_at_t = np.array([1.0, 0.0, 0.0])
        # spline_measurement = np.concatenate((d_pos, der_at_t))
        # observation = np.concatenate((spline_measurement, self.state[2:5]))
        # d_pos = self.state[0:3] - self.spl_projections[self.ref_ind_cur]
        # d_pos = self.state[0:3] - self.ref_pose
        # observation = np.concatenate((observation, d_pos[0:2]))
        return observation

    def set_action_sequence(self, action_sequence):
        # self.action_sequence[self.prediction_len:2*self.prediction_len] = action_sequence
        self.action_sequence = np.concatenate((np.zeros(self.prediction_len), action_sequence, np.zeros(self.prediction_len)))
        self.spline_func, self.quad_path, self.spl_projections, self.max_t = \
            DroneModelNlinV3.get_b_spline(self.action_sequence, self.spline_degree)
        self.spline_der = self.spline_func.derivative()
        self.t_param = 0.0
        self.t_old = 0.0
        self.ref_ind_cur = 0
        self.ref_pose = np.array([0.0, 0.0, 1.0])
        self.state = self.reset_state
        ref_actions = self.get_ref_actions()
        observation = np.concatenate((ref_actions.astype(dtype=np.float32), self.state[2:6]))
        # d_pos = self.state[0:3] - self.spline_func(0.0)
        # der_at_t = np.array([1.0, 0.0, 0.0])
        # spline_measurement = np.concatenate((d_pos, der_at_t))
        # observation = np.concatenate((spline_measurement, self.state[2:5]))
        # d_pos = self.state[0:3] - self.spl_projections[self.ref_ind_cur]
        # d_pos = self.state[0:3] - self.ref_pose
        # observation = np.concatenate((observation, d_pos[0:2]))
        return observation

    def get_closest_t(self):

        search_range = np.linspace(self.t_param - 1.0, self.t_param + 1.0, 1000)  # TODO: search parameter can slow down things
        search_range = np.clip(search_range, 0.0, self.max_t)

        t_poses = self.spline_func(search_range)
        distances = np.linalg.norm(t_poses - self.state[0:3], axis=1)
        min_indx = np.argmin(distances)

        self.t_param = search_range[min_indx]
        return self.t_param, distances[min_indx]

    def get_ref_actions(self):

        discrete_tangent_vec = self.spl_projections[self.ref_ind_cur + 1] - self.spl_projections[self.ref_ind_cur]
        quad_vec = self.state[0:3] - self.spl_projections[self.ref_ind_cur]
        quad_vec_proj = np.dot(quad_vec, discrete_tangent_vec) / np.linalg.norm(discrete_tangent_vec)
        if quad_vec_proj > np.linalg.norm(discrete_tangent_vec): # TODO: RL might go back, should we encount this ?
            self.ref_ind_cur += 1
            self.ref_pose = self.state[0:3]
        elif quad_vec_proj < 0.0:
            self.ref_ind_cur -= 1

        if self.ref_ind_cur < 0:
            self.ref_ind_cur = 0

        if (self.ref_ind_cur + self.prediction_len) < self.action_sequence.shape[0]:
            ref_actions = self.action_sequence[self.ref_ind_cur: self.ref_ind_cur + self.prediction_len]
        else:
            ref_actions = np.concatenate((self.action_sequence[self.ref_ind_cur: self.action_sequence.shape[0]],
                                         np.zeros(self.prediction_len - (self.action_sequence.shape[0] - self.ref_ind_cur), dtype=np.int)))

        return ref_actions

    @staticmethod
    def get_b_spline(action_sequence, degree):
        quad_pose = np.array([0.0, 0.0, 1.0])
        quad_path = np.zeros((np.shape(action_sequence)[0] + 1, 3))
        quad_path[0, :] = quad_pose

        for it_actions in range(np.shape(action_sequence)[0]):
            quad_pose[0:2] += DroneModelNlinV3.action_2_d_path(action_sequence[it_actions])
            quad_path[it_actions + 1, :] = quad_pose

        quad_path = np.array(quad_path)
        count = quad_path.shape[0]
        knots = np.clip(np.arange(count + degree + 1) - degree, 0, count - degree)

        spl = si.BSpline(knots, quad_path, degree)
        max_t = count - degree

        # search closest points on spline
        t_search = 0.0
        dt = float(max_t)/(np.shape(action_sequence)[0])
        spl_projections = np.zeros((np.shape(quad_path)[0], 3))

        for it_points in range(np.shape(quad_path)[0]):
            search_range = np.linspace(t_search, t_search + 2.0*dt, 200)
            search_range = np.clip(search_range, 0.0, max_t)

            t_poses = spl(search_range)
            distances = np.linalg.norm(t_poses - quad_path[it_points, :], axis=1)
            min_indx = np.argmin(distances)
            t_search = search_range[min_indx]
            spl_point = spl(t_search)
            spl_projections[it_points, :] = spl_point

        return spl, quad_path, spl_projections, max_t

    @staticmethod
    def action_2_d_path(action):
        return {
            0: np.array([0.5, 0.0]),
            1: np.array([0.353, 0.353]),
            2: np.array([0.0, 0.5]),
            3: np.array([-0.353, 0.353]),
            4: np.array([-0.5, 0.0]),
            5: np.array([-0.353, -0.353]),
            6: np.array([0.0, -0.5]),
            7: np.array([0.353, -0.353]),
        }[action]

