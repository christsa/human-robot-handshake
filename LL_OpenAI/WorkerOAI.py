import numpy as np
import tensorflow as tf
from collections import deque
from LL_OpenAI.Model_MLP import Model_MLP
import matplotlib.pyplot as plt
import time
import pdb


class WorkerOAI(object):

    def __init__(self, env, model, nsteps, model_path, gamma=0.99, lam=0.95, splines=None):
        self.env = env
        self.model = model # type: Model_MLP
        #self.obs = np.zeros(len(env.observation_space.high))
        self.obs = np.zeros((1,) + (self.model.sensor_size,), dtype=model.train_model.X.dtype.name)
        self.obs[:] = self.env.reset()
        self.gamma = gamma
        self.lam = lam
        self.nsteps = nsteps
        self.states = self.model.initial_state # This is for recurrent networks
        # self.dones = [False for _ in range(nenv)] # This is array to check if multiple envs are done
        self.model_path = model_path
        self.summary_writer = tf.summary.FileWriter("worker_ppo")
        self.dones = [False for _ in range(1)] # TODO: just for one env
        self.splines = splines

    def run(self, sess, update):
        mb_obs, mb_rewards, mb_actions, mb_values, mb_dones, mb_neglogpacs = [],[],[],[],[],[]
        mb_states = self.states
        epinfos = []
        first_run = False
        count = 0
        log = []
        log.append(np.array([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]))
        for _ in range(self.nsteps):
            actions, values, self.states, neglogpacs = self.model.step(self.obs, self.states, self.dones)
            # actions, values, self.states, neglogpacs = self.model.step(sess, self.obs)
            mb_obs.append(self.obs.copy())
            mb_actions.append(actions)
            mb_values.append(values)
            mb_neglogpacs.append(neglogpacs)
            mb_dones.append(self.dones)
            self.obs[:], rewards, self.dones, infos = self.env.step(actions)
            for info in infos:
                if first_run:
                    state_log = info.get('state')
                    log.append(state_log)
            for info in infos:
                maybeepinfo = info.get('episode')
                if maybeepinfo: epinfos.append(maybeepinfo)
            if self.dones:
                if self.splines is not None:
                    action_sequence_num = np.random.randint(0, np.shape(self.splines)[0])
                    # action_sequence_num = 4
                    env = self.env.venv.envs[0].env # TODO: complex, need to change
                    self.obs[:] = env.set_action_sequence(self.splines[action_sequence_num, :])
                if count == 0:
                    first_run = True
                else:
                    first_run = False
                count += 1
            mb_rewards.append(rewards)
        # if (update % 76 == 0):
        #     log = np.array(log)
        #     print(log)
        #     plt.figure(1)
        #     plt.plot(log[:, 0], log[:, 1])
        #     plt.figure(2)
        #     plt.plot(log[:, 3])
        #     plt.figure(3)
        #     plt.plot(log[:, 2])
        #     plt.show()
        #batch of steps to batch of rollouts
        mb_obs = np.asarray(mb_obs, dtype=self.obs.dtype)
        mb_rewards = np.asarray(mb_rewards, dtype=np.float32)
        mb_actions = np.asarray(mb_actions)
        mb_values = np.asarray(mb_values, dtype=np.float32)
        mb_neglogpacs = np.asarray(mb_neglogpacs, dtype=np.float32)
        mb_dones = np.asarray(mb_dones, dtype=np.bool)
        last_values = self.model.value(self.obs, self.states, self.dones)
        #last_values = self.model.get_value(sess, self.obs)
        #discount/bootstrap off value fn
        mb_returns = np.zeros_like(mb_rewards)
        mb_advs = np.zeros_like(mb_rewards)
        lastgaelam = 0
        for t in reversed(range(self.nsteps)):
            if t == self.nsteps - 1:
                nextnonterminal = 1.0 - self.dones
                nextvalues = last_values
            else:
                nextnonterminal = 1.0 - mb_dones[t+1]
                nextvalues = mb_values[t+1]
            delta = mb_rewards[t] + self.gamma * nextvalues * nextnonterminal - mb_values[t]
            mb_advs[t] = lastgaelam = delta + self.gamma * self.lam * nextnonterminal * lastgaelam
        mb_returns = mb_advs + mb_values

        mb_obs = WorkerOAI.sf01(mb_obs)
        mb_returns = WorkerOAI.sf01(mb_returns)
        mb_actions = WorkerOAI.sf01(mb_actions)
        mb_values = WorkerOAI.sf01(mb_values)
        mb_neglogpacs = WorkerOAI.sf01(mb_neglogpacs)
        mb_dones = WorkerOAI.sf01(mb_dones)

        return mb_obs, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs, mb_states, epinfos

        # return (*map(self.sf01, (mb_obs, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs)),
        #     mb_states, epinfos)
        # return (mb_obs, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs, mb_states, epinfos)

    def work(self, sess, saver, total_timesteps, lr, log_interval=10, nminibatches=4, noptepochs=4,
             cliprange=0.2, save_interval=0):

        if isinstance(lr, float):
            lr = self.constfn(lr)
        else:
            assert callable(lr)
        if isinstance(cliprange, float):
            cliprange = self.constfn(cliprange)
        else:
            assert callable(cliprange)
        total_timesteps = int(total_timesteps)

        # nenvs = self.env.num_envs
        nenvs = 1
        nbatch = nenvs * self.nsteps
        nbatch_train = nbatch // nminibatches

        epinfobuf = deque(maxlen=100)
        tfirststart = time.time()

        nupdates = total_timesteps // nbatch
        for update in range(1, nupdates + 1):
            assert nbatch % nminibatches == 0
            nbatch_train = nbatch // nminibatches
            tstart = time.time()
            frac = 1.0 - (update - 1.0) / nupdates
            lrnow = lr(frac)
            cliprangenow = cliprange(frac)
            obs, returns, masks, actions, values, neglogpacs, states, epinfos = self.run(sess, update)
            print(np.shape(obs))
            epinfobuf.extend(epinfos)
            mblossvals = []
            if states is None:  # nonrecurrent version #
                inds = np.arange(nbatch)
                for _ in range(noptepochs):
                    np.random.shuffle(inds)
                    for start in range(0, nbatch, nbatch_train):
                        end = start + nbatch_train
                        mbinds = inds[start:end]
                        slices = (arr[mbinds] for arr in (obs, returns, masks, actions, values, neglogpacs))
                        mblossvals.append(self.model.train(lrnow, cliprangenow, *slices))

            else:  # recurrent version
                # assert nenvs % nminibatches == 0
                # envsperbatch = nenvs // nminibatches
                # envinds = np.arange(nenvs)
                # flatinds = np.arange(nenvs * self.nsteps).reshape(nenvs, self.nsteps)
                # envsperbatch = nbatch_train // self.nsteps
                # for _ in range(noptepochs):
                #     np.random.shuffle(envinds)
                #     for start in range(0, nenvs, envsperbatch):
                #         end = start + envsperbatch
                #         mbenvinds = envinds[start:end]
                #         mbflatinds = flatinds[mbenvinds].ravel()
                #         slices = (arr[mbflatinds] for arr in (obs, returns, masks, actions, values, neglogpacs))
                #         mbstates = states[mbenvinds]
                #         mblossvals.append(self.trainer.train(lrnow, cliprangenow, *slices, mbstates)) # TODO: if you use recurretn model use this
                k = 0

            # Periodically save model parameters, and summary statistics.
            if update % 5 == 0:
                #if update % 250 == 0:
                if update % 25 == 0:
                    saver.save(sess, self.model_path + '/model-' + str(update) + '.cptk')
                    print ("Saved Model")

                mean_reward = self.safemean([epinfo['r'] for epinfo in epinfobuf])
                print(mean_reward)
                mean_len = self.safemean([epinfo['l'] for epinfo in epinfobuf])
                mean_value = np.mean(values)
                mblossvals = np.array(mblossvals)
                mean_policy_loss = np.mean(mblossvals[:, 0])
                mean_value_loss = np.mean(mblossvals[:, 1])
                mean_entropy = np.mean(mblossvals[:, 2])
                mean_aproxkl = np.mean(mblossvals[:, 3])
                mean_clipfrac = np.mean(mblossvals[:, 4])
                summary = tf.Summary()
                summary.value.add(tag='Perf/Reward', simple_value=float(mean_reward))
                summary.value.add(tag='Perf/Value', simple_value=float(mean_value))
                summary.value.add(tag='Perf/Length', simple_value=float(mean_len))
                summary.value.add(tag='Losses/Value Loss', simple_value=float(mean_value_loss))
                summary.value.add(tag='Losses/Policy Loss', simple_value=float(mean_policy_loss))
                summary.value.add(tag='Losses/Entropy', simple_value=float(mean_entropy))
                summary.value.add(tag='Losses/Aprox KL', simple_value=float(mean_aproxkl))
                summary.value.add(tag='Losses/Clipfrac', simple_value=float(mean_clipfrac))
                self.summary_writer.add_summary(summary, global_step=update)

                if mean_value_loss > 10000.0:
                    print (mean_value_loss)
                    print (list(returns))
                    print (list(values))
                    print (list(mblossvals[:, 1]))
                    print ("op maco")

                self.summary_writer.flush()

    @staticmethod
    def sf01(arr):
        """
        swap and then flatten axes 0 and 1
        """
        s = arr.shape
        return arr.swapaxes(0, 1).reshape(s[0] * s[1], *s[2:])

    @staticmethod
    def constfn(val):
        def f(_):
            return val
        return f

    @staticmethod
    def safemean(xs):
        return np.nan if len(xs) == 0 else np.mean(xs)
