import sys
sys.path.append('/home/sammy/Downloads/hierarchical_TL-python_2/HL')
from ArchitectureNN import ArchitectureNN
from distributions import make_pdtype

import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

from helper import *


class OneHiddenFullStohastic(ArchitectureNN):
    def __init__(self, sensor_size, action_size, scope):

        super(OneHiddenFullStohastic, self).__init__(action_size, scope)

        with tf.variable_scope(self.scope):
            self.inputs = tf.placeholder(shape=[None, sensor_size], dtype=tf.float32)
            # hidden = slim.fully_connected(self.inputs, 256, activation_fn=tf.nn.elu, reuse=None, scope="hidden") # maybe activation relu and 512 activation
            hidden = slim.fully_connected(self.inputs, 256, activation_fn=tf.nn.relu, reuse=None,
                                          scope="hidden")

            self.policy = slim.fully_connected(hidden, 2*self.action_size,
                                               activation_fn=None,
                                               weights_initializer=normalized_columns_initializer(0.01),
                                               biases_initializer=tf.constant_initializer(0.0),
                                               reuse=None,
                                               scope="output")

            self.value = slim.fully_connected(hidden, 1,
                                              activation_fn=None,
                                              weights_initializer=normalized_columns_initializer(1.0),
                                              biases_initializer=tf.constant_initializer(0.0),
                                              scope="value_out")

            # this part somehow add stohasticity
            self.pdtype = make_pdtype(self.action_size)
            self.pd = self.pdtype.pdfromflat(self.policy)

            self.a0 = self.pd.sample()
            self.neglogp0 = self.pd.neglogp(self.a0)
            self.initial_state = None  # recurrent initial state

    def step(self, sess, observation):
        a, v, neglogp = sess.run([self.a0, self.value, self.neglogp0], {self.inputs: [observation]})
        return a[0], v[0, 0], self.initial_state, neglogp[0]

    def get_value(self, sess, observation):
        return sess.run(self.value, {self.inputs: [observation]})[0]
