import numpy as np
import tensorflow as tf
from TwoHiddenFullStohastic import TwoHiddenFullStohastic


class TrainerPPO(object):
    def __init__(self, network, ent_coef, vf_coef=0.5, max_grad_norm=0.5):

        self.train_model = network # type: TwoHiddenFullStohastic # TODO: need base class

        with tf.variable_scope(self.train_model.scope):

            self.A = self.train_model.pdtype.sample_placeholder([None])
            self.ADV = tf.placeholder(tf.float32, [None])
            self.R = tf.placeholder(tf.float32, [None])
            self.OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
            self.OLDVPRED = tf.placeholder(tf.float32, [None])
            self.LR = tf.placeholder(tf.float32, [])
            self.CLIPRANGE = tf.placeholder(tf.float32, [])

            neglogpac = self.train_model.pd.neglogp(self.A)
            self.entropy = tf.reduce_mean(self.train_model.pd.entropy())

            vpred = self.train_model.value
            vpredclipped = self.OLDVPRED + tf.clip_by_value(self.train_model.value - self.OLDVPRED,
                                                            - self.CLIPRANGE, self.CLIPRANGE)
            vf_losses1 = tf.square(vpred - self.R)
            vf_losses2 = tf.square(vpredclipped - self.R)
            self.vf_loss = .5 * tf.reduce_mean(tf.maximum(vf_losses1, vf_losses2))
            # self.vf_loss = .5 * tf.reduce_mean(vf_losses1) # no loss clipping!
            ratio = tf.exp(self.OLDNEGLOGPAC - neglogpac)
            pg_losses = -self.ADV * ratio
            pg_losses2 = -self.ADV * tf.clip_by_value(ratio, 1.0 - self.CLIPRANGE, 1.0 + self.CLIPRANGE)
            self.pg_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
            self.approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - self.OLDNEGLOGPAC))
            self.clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), self.CLIPRANGE)))
            loss = self.pg_loss - self.entropy * ent_coef + self.vf_loss * vf_coef
            with tf.variable_scope(self.train_model.scope):
                params = tf.trainable_variables()
            grads = tf.gradients(loss, params)
            if max_grad_norm is not None:
                grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
            grads = list(zip(grads, params))
            trainer = tf.train.AdamOptimizer(learning_rate=self.LR, epsilon=1e-5)
            self._train = trainer.apply_gradients(grads)

    def train(self, sess, lr, cliprange, obs, returns, masks, actions, values, neglogpacs, states=None):
        advs = returns - values
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)
        td_map = {self.train_model.inputs: obs, self.A: actions, self.ADV: advs, self.R: returns, self.LR: lr,
                  self.CLIPRANGE: cliprange, self.OLDNEGLOGPAC: neglogpacs, self.OLDVPRED: values}
        if states is not None: # not using recurent verison for now, can stay for latter
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks
        return sess.run(
            [self.pg_loss, self.vf_loss, self.entropy, self.approxkl, self.clipfrac, self._train],
            td_map
        )[:-1]
