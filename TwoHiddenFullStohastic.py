import sys
sys.path.append('/home/sammy/Downloads/hierarchical_TL-python_2/HL')
from ArchitectureNN import ArchitectureNN
from distributions import make_pdtype

import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

from helper import *


class TwoHiddenFullStohastic(ArchitectureNN):
    def __init__(self, sensor_size, action_size, scope):

        super(TwoHiddenFullStohastic, self).__init__(action_size, scope)

        with tf.variable_scope(self.scope):
            self.inputs = tf.placeholder(shape=[None, sensor_size], dtype=tf.float32)
            hidden_1_policy = slim.fully_connected(self.inputs, 64, activation_fn=tf.nn.tanh, reuse=None,
                                                   weights_initializer=ortho_init(scale=np.sqrt(2)),  # TODO: this is used from PPO repo, be careful this is only defined for len(shape) = 2
                                                   biases_initializer=tf.constant_initializer(0.0),
                                                   scope="hidden_policy_1")
            hidden_2_policy = slim.fully_connected(hidden_1_policy, 64, activation_fn=tf.nn.tanh, reuse=None,
                                                   weights_initializer=ortho_init(scale=np.sqrt(2)),
                                                   biases_initializer=tf.constant_initializer(0.0),
                                                   scope="hidden_policy_2")

            self.policy = slim.fully_connected(hidden_2_policy, self.action_size,
                                               activation_fn=None,
                                               weights_initializer=ortho_init(scale=0.01),
                                               biases_initializer=tf.constant_initializer(0.0),
                                               reuse=None,
                                               scope="output")
            logstd = tf.get_variable(name="logstd", shape=[1, self.action_size], initializer=tf.zeros_initializer())

            hidden_1_value = slim.fully_connected(self.inputs, 64, activation_fn=tf.nn.tanh, reuse=None,
                                                  weights_initializer=ortho_init(scale=np.sqrt(2)),
                                                  biases_initializer=tf.constant_initializer(0.0),
                                                  scope="hidden_value_1")
            hidden_2_value = slim.fully_connected(hidden_1_value, 64, activation_fn=tf.nn.tanh, reuse=None,
                                                  weights_initializer=ortho_init(scale=np.sqrt(2)),
                                                  biases_initializer=tf.constant_initializer(0.0),
                                                  scope="hidden_value_2")
            self.value = slim.fully_connected(hidden_2_value, 1,
                                              activation_fn=None,
                                              weights_initializer=ortho_init(scale=1.00),
                                              biases_initializer=tf.constant_initializer(0.0),
                                              scope="value_out")

            pdparam = tf.concat([self.policy, self.policy * 0.0 + logstd], axis=1)

            # this part somehow add stohasticity
            self.pdtype = make_pdtype(self.action_size)
            self.pd = self.pdtype.pdfromflat(pdparam)

            self.a0 = self.pd.sample()
            self.neglogp0 = self.pd.neglogp(self.a0)
            self.initial_state = None  # recurrent initial state

    def step(self, sess, observation):
        a, v, neglogp = sess.run([self.a0, self.value, self.neglogp0], {self.inputs: [observation]})
        return a[0], v[0, 0], self.initial_state, neglogp[0]

    def get_value(self, sess, observation):
        return sess.run(self.value, {self.inputs: [observation]})[0]
