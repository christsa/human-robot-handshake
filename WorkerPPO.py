import numpy as np
import tensorflow as tf
from collections import deque
from TrainerPPO import TrainerPPO
from OneHiddenFullStohastic import OneHiddenFullStohastic
import time
import pdb


class WorkerPPO(object):

    def __init__(self, env, trainer, nsteps, model_path, gamma=0.99, lam=0.95):
        self.env = env
        self.trainer = trainer  # type: TrainerPPO
        self.model = trainer.train_model  # type: TwoHiddenFullStohastic
        # nenv = env.num_envs # TODO: can we do in paralel
        # self.obs = np.zeros((nenv,) + env.observation_space.shape, dtype=model.train_model.X.dtype.name) # TODO: 2 env - do we define space as in open AI lab
        # self.obs = np.zeros(env.state_size)  # TODO: 2 env - do we define space as in open AI lab - dtype float for now - need to be corrected for generality
        # self.obs = np.zeros(len(env.observation_space.high), dtype=self.model.inputs.dtype.name)
        self.obs = np.zeros(len(env.observation_space.high))
        #self.obs = np.zeros(len(env.observation_space.high))
        self.obs[:] = self.env.reset()
        self.gamma = gamma
        self.lam = lam
        self.nsteps = nsteps
        self.states = self.model.initial_state # This is for recurrent networks
        # self.dones = [False for _ in range(nenv)] # This is array to check if multiple envs are done
        self.dones = False
        self.model_path = model_path
        self.summary_writer = tf.summary.FileWriter("worker_ppo")

    def run(self, sess, update):
        mb_obs, mb_rewards, mb_actions, mb_values, mb_dones, mb_neglogpacs = [],[],[],[],[],[]
        episode_len, episode_reward = [], []
        episode_len_tmp = 0
        episode_reward_tmp = 0.0
        mb_states = self.states
        epinfos = []
        first_run = True
        for _ in range(self.nsteps):
            actions, values, self.states, neglogpacs = self.model.step(sess, self.obs) # if we use recurrent network, than we need additional inputs (inint state and mask)
            mb_obs.append(self.obs.copy())
            mb_actions.append(actions)
            mb_values.append(values)
            mb_neglogpacs.append(neglogpacs)
            mb_dones.append(self.dones)
            spline = 0  # TODO: need to be implemented
            #self.obs[:], rewards, self.dones, infos = self.env.step(spline, actions)
            if (update % 25 == 0) and first_run:
                self.env.render()
            self.obs[:], rewards, self.dones, infos = self.env.step(actions)
            episode_len_tmp += 1
            episode_reward_tmp += rewards
            # for info in infos: # I don't have info
            #     maybeepinfo = info.get('episode')
            #     if maybeepinfo: epinfos.append(maybeepinfo)
            mb_rewards.append(rewards)
            if self.dones:
                first_run = False
                self.obs[:] = self.env.reset()
                episode_len.append(episode_len_tmp)
                episode_reward.append(episode_reward_tmp)
                episode_len_tmp = 0
                episode_reward_tmp = 0.0
        episode_len.append(episode_len_tmp)
        episode_reward.append(episode_reward_tmp)
        if update % 25 == 0:
            print ("video reward")
        print (episode_reward)
        #batch of steps to batch of rollouts
        #mb_obs = np.asarray(mb_obs, dtype=self.obs.dtype)
        mb_obs = np.asarray(mb_obs, dtype=np.float32)
        mb_rewards = np.asarray(mb_rewards, dtype=np.float32)
        mb_actions = np.asarray(mb_actions)
        mb_values = np.asarray(mb_values, dtype=np.float32)
        mb_neglogpacs = np.asarray(mb_neglogpacs, dtype=np.float32)
        mb_dones = np.asarray(mb_dones, dtype=np.bool)
        mb_len = np.asarray(episode_len, dtype=np.float32)
        mb_ep_reward = np.asarray(episode_reward, dtype=np.float32)
        last_values = self.model.get_value(sess, self.obs) # if we use recurrent network, than we need additional inputs (inint state and mask)
        #discount/bootstrap off value fn
        mb_returns = np.zeros_like(mb_rewards)
        mb_advs = np.zeros_like(mb_rewards)
        lastgaelam = 0
        # calculates advantages
        for t in reversed(range(self.nsteps)): # trick for bootstraping to write it more compact
            if t == self.nsteps - 1:
                nextnonterminal = 1.0 - self.dones
                nextvalues = last_values
            else:
                nextnonterminal = 1.0 - mb_dones[t+1]
                nextvalues = mb_values[t+1]
            delta = mb_rewards[t] + self.gamma * nextvalues * nextnonterminal - mb_values[t] # They also use TD(lambda)
            mb_advs[t] = lastgaelam = delta + self.gamma * self.lam * nextnonterminal * lastgaelam # I think this is GAE
        mb_returns = mb_advs + mb_values

        # mb_obs = self.sf01(mb_obs)
        # mb_actions = self.sf01(mb_actions)
        # mb_values = self.sf01(mb_values)
        # mb_neglogpacs = self.sf01(mb_neglogpacs)
        # mb_dones = self.sf01(mb_dones)

        return mb_obs, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs, mb_states, epinfos, mb_rewards, \
               mb_len, mb_ep_reward

    def work(self, sess, saver, total_timesteps, lr, log_interval=10, nminibatches=4, noptepochs=4,
             cliprange=0.2, save_interval=0):

        # if isinstance(lr, float):
        #     lr = self.constfn(lr)
        # else:
        #     assert callable(lr)
        # if isinstance(cliprange, float):
        #     cliprange = self.constfn(cliprange)
        # else:
        #     assert callable(cliprange)
        total_timesteps = int(total_timesteps)

        # nenvs = self.env.num_envs
        nenvs = 1
        nbatch = nenvs * self.nsteps
        nbatch_train = nbatch // nminibatches

        epinfobuf = deque(maxlen=100)
        tfirststart = time.time()

        nupdates = total_timesteps // nbatch
        for update in range(1, nupdates + 1):
            assert nbatch % nminibatches == 0
            nbatch_train = nbatch // nminibatches
            tstart = time.time()
            # frac = 1.0 - (float(update) - 1.0) / float(nupdates)
            frac = 1.0
            # lrnow = lr(frac)
            # cliprangenow = cliprange(frac)
            lrnow = lr*frac
            cliprangenow = cliprange*frac
            obs, returns, masks, actions, values, neglogpacs, states, epinfos, rewards, len, ep_reward = self.run(sess, update)
            epinfobuf.extend(epinfos)
            mblossvals = []
            if states is None:  # nonrecurrent version #
                inds = np.arange(nbatch)
                for _ in range(noptepochs):
                    np.random.shuffle(inds)
                    for start in range(0, nbatch, nbatch_train):
                        end = start + nbatch_train
                        mbinds = inds[start:end]
                        # slices = (arr[mbinds] for arr in (obs, returns, masks, actions, values, neglogpacs))
                        obs_slices = obs[mbinds]
                        returns_slices = returns[mbinds]
                        actions_slices = actions[mbinds]
                        values_slices = values[mbinds]
                        neglogpacs_slices = neglogpacs[mbinds]

                        mblossvals.append(self.trainer.train(sess, lrnow, cliprangenow, obs_slices, returns_slices,
                                                             actions_slices, values_slices, neglogpacs_slices))
            else:  # recurrent version
                # assert nenvs % nminibatches == 0
                # envsperbatch = nenvs // nminibatches
                # envinds = np.arange(nenvs)
                # flatinds = np.arange(nenvs * self.nsteps).reshape(nenvs, self.nsteps)
                # envsperbatch = nbatch_train // self.nsteps
                # for _ in range(noptepochs):
                #     np.random.shuffle(envinds)
                #     for start in range(0, nenvs, envsperbatch):
                #         end = start + envsperbatch
                #         mbenvinds = envinds[start:end]
                #         mbflatinds = flatinds[mbenvinds].ravel()
                #         slices = (arr[mbflatinds] for arr in (obs, returns, masks, actions, values, neglogpacs))
                #         mbstates = states[mbenvinds]
                #         mblossvals.append(self.trainer.train(lrnow, cliprangenow, *slices, mbstates)) # TODO: if you use recurretn model use this
                k = 0

            # Periodically save model parameters, and summary statistics.
            if update % 5 == 0:
                if update % 250 == 0:
                    saver.save(sess, self.model_path + '/model-' + str(update) + '.cptk')
                    print ("Saved Model")

                mean_reward = np.mean(ep_reward) # TODO: this is just for one step, extend to all 5
                mean_len = np.mean(len)
                mean_value = np.mean(values)
                mblossvals = np.array(mblossvals)
                mean_policy_loss = np.mean(mblossvals[:, 0])
                mean_value_loss = np.mean(mblossvals[:, 1])
                mean_entropy = np.mean(mblossvals[:, 2])
                mean_aproxkl = np.mean(mblossvals[:, 3])
                mean_clipfrac = np.mean(mblossvals[:, 4])
                summary = tf.Summary()
                summary.value.add(tag='Perf/Reward', simple_value=float(mean_reward))
                summary.value.add(tag='Perf/Value', simple_value=float(mean_value))
                summary.value.add(tag='Perf/Length', simple_value=float(mean_len))
                summary.value.add(tag='Losses/Value Loss', simple_value=float(mean_value_loss))
                summary.value.add(tag='Losses/Policy Loss', simple_value=float(mean_policy_loss))
                summary.value.add(tag='Losses/Entropy', simple_value=float(mean_entropy))
                summary.value.add(tag='Losses/Aprox KL', simple_value=float(mean_aproxkl))
                summary.value.add(tag='Losses/Clipfrac', simple_value=float(mean_clipfrac))
                self.summary_writer.add_summary(summary, update)

                if mean_value_loss > 10000.0:
                    print (mean_value_loss)
                    print (list(returns))
                    print (list(values))
                    print (list(mblossvals[:, 1]))
                    print ("op maco")

                self.summary_writer.flush()

    @staticmethod
    def sf01(arr):
        """
        swap and then flatten axes 0 and 1
        """
        s = arr.shape
        return arr.swapaxes(0, 1).reshape(s[0] * s[1], *s[2:])

    @staticmethod
    def constfn(val):
        def f(_):
            return val
        return f
