import numpy as np
from LL.DroneModelNlinV2 import DroneModelNlinV2
dt = 0.1
model_params = {'alpha': 0.85,
                'C': 0.57,
                'prediction_len': 4}

game = DroneModelNlinV2(dt, params=model_params)

test_u = np.array([0.0, -1.0, .0])
action_sequence = np.array([0, 1, 1, 0])
game.set_action_sequence(action_sequence)

quad_pose = np.array([0.0, 0.0, 1.0])
quad_path = np.zeros((np.shape(action_sequence)[0] + 1, 3))
quad_path[0, :] = quad_pose

for it_actions in range(np.shape(action_sequence)[0]):
    quad_pose[0:2] += DroneModelNlinV2.action_2_d_path(action_sequence[it_actions])
    quad_path[it_actions + 1, :] = quad_pose

quad_path = np.array(quad_path)

import matplotlib.pyplot as plt
t = np.linspace(0, 8, 1000)
y = game.spline_func(t)
plt.figure(1)
plt.plot(quad_path[:, 0], quad_path[:, 1], '-o')
plt.plot(y[:, 0], y[:, 1],)
plt.show()
