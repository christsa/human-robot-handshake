import scipy.interpolate as si
import numpy as np
import matplotlib.pyplot as plt

test_points = np.array([0, 1, 2])
c = np.array([[0, 0],
              [0.5, 0],
              [1.0, 0],
              [1.353, 0.353],
              [1.353, 0.835],
              [1.706, 0.835]])
# c = np.array([0, 0, 1, 0, 0])
count = c.shape[0]
print(count)
degree = 3
knots = np.clip(np.arange(count+degree+1)-degree, 0, count-degree)
print(knots)

spl = si.BSpline(knots, c, degree)
derivative = spl.derivative()

t = np.linspace(0, count - degree, 100)
y = spl(t)
der = derivative(t)
plt.figure(1)
plt.plot(y[:, 0], y[:, 1])
plt.plot(c[:, 0], c[:, 1])
plt.plot(der[:, 0], der[:, 1])
# plt.plot(test_points, c)
# plt.plot(t, y, '-x')
plt.show()
# plt.figure(1)
# plt.plot(t, y)
# plt.plot(t, der)
# # plt.plot(test_points, c)
# # plt.plot(t, y, '-x')
# plt.show()