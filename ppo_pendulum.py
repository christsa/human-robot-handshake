import sys
sys.path.append('/home/sammy/Downloads/hierarchical_TL-python_2/HL')
sys.path.append('/home/sammy/Downloads/hierarchical_TL-python_2/LL/LL_OpenAI')
sys.path.append('/home/sammy/Downloads/hierarchical_TL-python_2/LL/common')
sys.path.append('/home/sammy/Downloads/hierarchical_TL-python_2/LL/bench')
import tensorflow as tf
import os
from OneHiddenFullStohastic import OneHiddenFullStohastic
from TwoHiddenFullStohastic import TwoHiddenFullStohastic
from TrainerPPO import TrainerPPO
from WorkerPPO import WorkerPPO
from DroneModelNlin import DroneModelNlin
from LL_OpenAI import WorkerOAI
from LL_OpenAI import Model_MLP
import gym
import bench
import logger
from common.vec_env.dummy_vec_env import DummyVecEnv
from common.vec_env.vec_normalize import VecNormalize
import pdb

gamma = .99  # discount rate for advantage estimation and reward discounting
lam = .95
num_timesteps = 1000000
load_model = False
model_path = './model'

dt = 0.1
model_params = {'alpha': 0.85,
                'C': 0.57}

tf.reset_default_graph()
 
if not os.path.exists(model_path):
    os.makedirs(model_path)

with tf.device("/cpu:0"):
    global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4) #learning_rate=1e-4
    # game = DroneModelNlin(dt, params=model_params)
    # a_size = game.action_size
    # s_size = game.state_size
    game_test = gym.make('Pendulum-v0')
    #game_test = gym.make('HandReach-v0')
    a_size = len(game_test.action_space.high)
    #s_size = game_test.observation_space.spaces.get('observation').high.size
    s_size = len(game_test.observation_space.high)
    def make_env():
        game = gym.make('Pendulum-v0')
        game = bench.Monitor(game, logger.get_dir())
        return game
    game = DummyVecEnv([make_env])
    game = VecNormalize(game)

    # #network = OneHiddenFullStohastic(s_size, a_size, 'model')
    # network = TwoHiddenFullStohastic(s_size, a_size, 'model')
    # # num_workers = multiprocessing.cpu_count()  # Set workers ot number of available CPU threads
    # trainer = TrainerPPO(network, ent_coef=0.0)

with tf.Session() as sess:
    nminibatches = 32
    nsteps = 2048
    nbatch = 1 * nsteps
    nbatch_train = nbatch // nminibatches
    model = Model_MLP.Model_MLP(sess, s_size, a_size, 1, nbatch_train, nsteps, ent_coef=0.0, vf_coef=0.5,  max_grad_norm=0.5) # nsteps is not used i MPL
    worker = WorkerOAI.WorkerOAI(game, model, model_path=model_path, nsteps=nsteps, gamma=gamma, lam=lam)
    saver = tf.train.Saver(max_to_keep=5)

    coord = tf.train.Coordinator()
    if load_model == True:
        print ('Loading Model...')
        ckpt = tf.train.get_checkpoint_state(model_path)
        saver.restore(sess, ckpt.model_checkpoint_path)
    else:
        sess.run(tf.global_variables_initializer())

    # This is where the asynchronous magic happens.
    # Start the "work" process for each worker in a separate threat.
    worker.work(sess, saver, total_timesteps=num_timesteps, lr=3e-4, log_interval=10, nminibatches=32, noptepochs=10, # nminibatches=4 can change value to make it easier fpr training
                cliprange=0.2, save_interval=0)
