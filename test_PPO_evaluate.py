import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
from LL.LL_OpenAI.WorkerOAI import WorkerOAI
from LL.LL_OpenAI.Model_MLP import Model_MLP
import gym
from LL import bench
from LL.common.vec_env.dummy_vec_env import DummyVecEnv
from LL.common.vec_env.vec_normalize import VecNormalize
from LL.DroneModelNlin import DroneModelNlin
gamma = .99  # discount rate for advantage estimation and reward discounting
lam = .95
num_timesteps = 1000 * 100000
load_model = True
model_path = './model'

dt = 0.1
model_params = {'alpha': 0.0,
                'C': 0.0}

tf.reset_default_graph()

if not os.path.exists(model_path):
    os.makedirs(model_path)

with tf.device("/cpu:0"):
    global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4) #learning_rate=1e-4
    game_to_use = DroneModelNlin(dt, params=model_params)
    game_test = game_to_use
    a_size = game_test.action_size
    s_size = game_test.state_size

    def make_env():
        game = game_to_use
        game = bench.MyMonitor(game)
        return game
    game = DummyVecEnv([make_env])
    game = VecNormalize(game)

with tf.Session() as sess:
    nminibatches = 32
    nsteps = 2048
    nbatch = 1 * nsteps
    nbatch_train = nbatch // nminibatches
    model = Model_MLP(sess, s_size, a_size, 1, nbatch_train, nsteps, ent_coef=0.0, vf_coef=0.5,  max_grad_norm=0.5) # nsteps is not used i MPL
    worker = WorkerOAI(game, model, model_path=model_path, nsteps=nsteps, gamma=gamma, lam=lam)
    saver = tf.train.Saver(max_to_keep=5)

    print('Loading Model...')
    ckpt = tf.train.get_checkpoint_state(model_path)
    saver.restore(sess, ckpt.model_checkpoint_path)

    with tf.variable_scope("model", reuse=tf.AUTO_REUSE):
        layer = sess.run(tf.get_variable("pi_fc1/w"))
        print(layer)

    log = []
    reward_sum = 0.0
    info = {'state': np.array([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0])}
    for i_episode in range(1):
        observation = game_test.reset()
        for t in range(1000):  # TODO: max_episode_len is never defined, maybe it can help
            log.append(info['state'])
            action, _, _, _ = model.step([observation])
            print(action)
            spline = 0
            observation, reward, done, info = game_test.step(action[0])
            reward_sum += reward
            print(reward)
            if done:
                log.append(info['state'])
                print("Episode finished after {} timesteps".format(t + 1))
                break

    print(reward_sum)
    log = np.array(log)
    plt.figure(1)
    plt.plot(log[:, 0], log[:, 1])
    plt.xlabel("x[m]")
    plt.ylabel("y[m]")
    plt.figure(2)
    plt.plot(log[:, 3])
    plt.xlabel("timestep")
    plt.ylabel("v_x[m/s]")
    plt.figure(3)
    plt.plot(log[:, 2])
    plt.xlabel("timestep")
    plt.ylabel("z[m]")
    plt.show()
