import tensorflow as tf
import numpy as np
import os
from LL.LL_OpenAI.WorkerOAI import WorkerOAI
from LL.LL_OpenAI.Model_MLP import Model_MLP
import matplotlib.pyplot as plt
import gym
from LL import bench
from LL.common.vec_env.dummy_vec_env import DummyVecEnv
from LL.common.vec_env.vec_normalize import VecNormalize
from LL.DroneModelNlinV2 import DroneModelNlinV2
from LL.DroneModelNlinV3 import DroneModelNlinV3
gamma = .99  # discount rate for advantage estimation and reward discounting
lam = .95
num_timesteps = 1000 * 100000
load_model = True
model_path = './model'

dt = 0.1
model_params = {'alpha': 0.63,
                'mass': 0.73,
                'C': 0.5,
                'prediction_len': 4}

splines = np.array([[0, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1],
                    [1, 0, 0, 0],
                    [1, 1, 0, 0],
                    [0, 0, 1, 1],
                    [0, 0, 2, 0],
                    [1, 2, 0, 0],
                    [0, 1, 2, 0],
                    [7, 0, 0, 0],
                    [0, 7, 0, 0],
                    [0, 0, 7, 0],
                    [0, 0, 0, 7],
                    [6, 7, 0, 0],
                    [0, 6, 7, 0],
                    [0, 0, 6, 7]])

tf.reset_default_graph()

if not os.path.exists(model_path):
    os.makedirs(model_path)

with tf.device("/cpu:0"):
    global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4) #learning_rate=1e-4
    # game_to_use = DroneModelNlinV2(dt, params=model_params)
    game_to_use = DroneModelNlinV3(dt, params=model_params)
    game = game_to_use
    a_size = game.action_size
    s_size = game.state_size

    # def make_env():
    #     game = game_to_use
    #     game = bench.MyMonitor(game)
    #     return game
    # game = DummyVecEnv([make_env])
    # game = VecNormalize(game, ob=False, ret=True)

with tf.Session() as sess:
    nminibatches = 32
    nsteps = 2048
    nbatch = 1 * nsteps
    nbatch_train = nbatch // nminibatches
    model = Model_MLP(sess, s_size, a_size, 1, nbatch_train, nsteps, ent_coef=0.0, vf_coef=0.5,  max_grad_norm=0.5) # nsteps is not used i MPL
    worker = WorkerOAI(game, model, model_path=model_path, nsteps=nsteps, gamma=gamma, lam=lam, splines=splines)
    saver = tf.train.Saver(max_to_keep=5)
    print('Loading Model...')
    ckpt = tf.train.get_checkpoint_state(model_path)
    saver.restore(sess, ckpt.model_checkpoint_path)

    log = []
    reward_sum = 0.0
    spline_num = 4
    t_mins = []
    # info = {'state': np.array([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0])}
    info = {'state': np.array([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])}
    for i_episode in range(1):
        observation = game.reset()
        # spline = splines[spline_num, :]
        # spline = np.array([0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 7, 0, 0, 6, 7, 0])
        spline = np.array([0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 6, 7, 0, 0])
        # spline = np.array([7, 0, 0, 1])
        observation = game.set_action_sequence(spline)
        for t in range(1000):  # TODO: max_episode_len is never defined, maybe it can help
            log.append(info['state'])
            action, _, _, _ = model.step([observation])
            spline = 0
            observation, reward, done, info = game.step(action[0])
            t_mins.append(info['t_min'])
            # action = np.array([.0, -1.0, 0.0])
            # observation, reward, done, info = game.step(action)
            reward_sum += reward
            if done:
                log.append(info['state'])
                print("Episode finished after {} timesteps".format(t + 1))
                break

    print(reward_sum)
    t_mins = np.array(t_mins)
    discrete_path = game.quad_path
    spl_path = game.spl_projections
    spline_f = game.spline_func
    t = np.linspace(0, game.max_t, 100)
    y = spline_f(t)
    t_poses = spline_f(t_mins)
    # der = game.spline_der(t)
    # manual_der = np.zeros(np.shape(der))
    # for it in range(np.shape(der)[0]):
    #     der[it, :] = der[it, :]/np.linalg.norm(der[it, :])
    #     if it < (np.shape(der)[0] - 1):
    #         dl = np.linalg.norm(y[it + 1, :] - y[it, :])
    #         dx = y[it + 1, 0] - y[it, 0]
    #         dy = y[it + 1, 1] - y[it, 1]
    #         manual_der[it, :] = np.array([dx/dl, dy/dl, 0.0])

    log = np.array(log)
    plt.figure(1)
    plt.plot(log[:, 0], log[:, 1], '-x')
    plt.plot(discrete_path[:, 0], discrete_path[:, 1], '-o')
    plt.plot(spl_path[:, 0], spl_path[:, 1], '-o')
    plt.plot(y[:, 0], y[:, 1])
    plt.plot(t_poses[:, 0], t_poses[:, 1], '-*')
    # plt.axis([0, 5, -2.5, 2.5])
    plt.xlabel("x[m]")
    plt.ylabel("y[m]")
    plt.figure(2)
    plt.plot(log[:, 3])
    plt.xlabel("timestep")
    plt.ylabel("v_x[m/s]")
    plt.figure(3)
    plt.plot(log[:, 2])
    plt.xlabel("timestep")
    plt.ylabel("z[m]")
    # plt.figure(4)
    # plt.plot(t, der[:, 0])
    # plt.plot(t, der[:, 1])
    # plt.plot(t, der[:, 2])
    # plt.plot(t, manual_der[:, 0])
    # plt.plot(t, manual_der[:, 1])
    # plt.plot(t, manual_der[:, 2])
    plt.show()