import tensorflow as tf
import os
from OneHiddenFullStohastic import OneHiddenFullStohastic
from TwoHiddenFullStohastic import TwoHiddenFullStohastic
from TrainerPPO import TrainerPPO
from WorkerPPO import WorkerPPO
from DroneModelNlin import DroneModelNlin
import gym
import time
import pdb

gamma = .99   # discount rate for advantage estimation and reward discounting
lam = .95
num_timesteps = 1000 * 100000
load_model = True
model_path = './model'

dt = 0.1
model_params = {'alpha': 0.85,
                'C': 0.57}
game_name = 'Pendulum-v0'

tf.reset_default_graph()

if not os.path.exists(model_path):
    os.makedirs(model_path)

with tf.device("/cpu:0"):
    global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4) #learning_rate=1e-4
    game = gym.make(game_name)
    a_size = len(game.action_space.high)
    s_size = len(game.observation_space.high)
    #network = OneHiddenFullStohastic(s_size, a_size, 'model')
    network = TwoHiddenFullStohastic(s_size, a_size, 'model')
    # num_workers = multiprocessing.cpu_count()  # Set workers ot number of available CPU threads
    trainer = TrainerPPO(network, ent_coef=0.0)
    worker = WorkerPPO(game, trainer, model_path=model_path, nsteps=128, gamma=gamma, lam=lam)
    saver = tf.train.Saver(max_to_keep=5)

with tf.Session() as sess:
    coord = tf.train.Coordinator()
    if load_model:
       ckpt = tf.train.get_checkpoint_state(model_path)
       OLD_CHECKPOINT_FILE = ckpt.model_checkpoint_path
       NEW_CHECKPOINT_FILE = ".../model.ckpt-2000000"

       vars_to_rename = {
       "model/hidden_policy_1/biases",
       }
       new_checkpoint_vars = {}
       reader = tf.train.NewCheckpointReader(OLD_CHECKPOINT_FILE)
       for old_name in reader.get_variable_to_shape_map():
         if old_name in vars_to_rename:
           new_name = vars_to_rename[old_name]
         else:
           new_name = old_name
         new_checkpoint_vars[new_name] = tf.Variable(reader.get_tensor(old_name))

       init = tf.global_variables_initializer()
       saver = tf.train.Saver(new_checkpoint_vars)

 
       sess.run(init)
       saver.save(sess, NEW_CHECKPOINT_FILE)

       #pdb.set_trace()
       print ('Loading Model...')
       ckpt = tf.train.get_checkpoint_state(model_path)
       saver.restore(sess, ckpt.model_checkpoint_path)
    else:
       sess.run(tf.global_variables_initializer())

    test_game = gym.make(game_name)
    for i_episode in range(1):
        observation = test_game.reset()
        for t in range(500):
            test_game.render()
            if (t == 0):
                time.sleep(1.5)
            action, _, _, _ = network.step(sess, observation)
            print ("action:", action)
            observation, reward, done, info = test_game.step(action)
            # print ("reward:", reward)
            if done:
                print("Episode finished after {} timesteps".format(t + 1))
                break
