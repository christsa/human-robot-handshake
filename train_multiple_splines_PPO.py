import tensorflow as tf
import numpy as np
import os
from LL.LL_OpenAI.WorkerOAI import WorkerOAI
from LL.LL_OpenAI.Model_MLP import Model_MLP
import gym
from LL import bench
from LL.common.vec_env.dummy_vec_env import DummyVecEnv
from LL.common.vec_env.vec_normalize import VecNormalize
from LL.DroneModelNlinV2 import DroneModelNlinV2
from LL.DroneModelNlinV3 import DroneModelNlinV3
gamma = .99  # discount rate for advantage estimation and reward discounting
# gamma = 0.8
lam = .95
num_timesteps = 1000 * 100000
load_model = True
model_path = './model'

dt = 0.1
model_params = {'alpha': 0.63,
                'mass': 0.73,
                'C': 0.5,
                'prediction_len': 4}

splines = np.array([[0, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1],
                    [1, 0, 0, 0],
                    [1, 1, 0, 0],
                    [0, 0, 1, 1],
                    [0, 0, 2, 0],
                    [1, 2, 0, 0],
                    [0, 1, 2, 0],
                    [7, 0, 0, 0],
                    [0, 7, 0, 0],
                    [0, 0, 7, 0],
                    [0, 0, 0, 7],
                    [6, 7, 0, 0],
                    [0, 6, 7, 0],
                    [0, 0, 6, 7]])

tf.reset_default_graph()

if not os.path.exists(model_path):
    os.makedirs(model_path)

with tf.device("/cpu:0"):
    global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4) #learning_rate=1e-4
    # game_to_use = DroneModelNlinV2(dt, params=model_params)
    game_to_use = DroneModelNlinV3(dt, params=model_params)
    game = game_to_use
    a_size = game.action_size
    s_size = game.state_size

    def make_env():
        game = game_to_use
        game = bench.MyMonitor(game)
        return game
    game = DummyVecEnv([make_env])
    game = VecNormalize(game, ob=False, ret=True)

with tf.Session() as sess:
    nminibatches = 32
    nsteps = 2048
    nbatch = 1 * nsteps
    nbatch_train = nbatch // nminibatches
    model = Model_MLP(sess, s_size, a_size, 1, nbatch_train, nsteps, ent_coef=0.0, vf_coef=0.5,  max_grad_norm=0.5) # nsteps is not used i MPL
    worker = WorkerOAI(game, model, model_path=model_path, nsteps=nsteps, gamma=gamma, lam=lam, splines=splines)
    saver = tf.train.Saver(max_to_keep=5)
    sess.run(tf.global_variables_initializer())

    worker.work(sess, saver, total_timesteps=num_timesteps, lr=3e-4, log_interval=10, nminibatches=nminibatches,
                noptepochs=10, cliprange=0.2, save_interval=0)
